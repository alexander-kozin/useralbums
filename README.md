
# README #
Application demonstrates working with API "https://jsonplaceholder.typicode.com".

### Features ###
* Display users list, user albums list and photos placeholders in albums (3 screens).
* Using ReactiveCocoa and MVVM template.
* Using NSOperation for asyncronious loading photos placeholders in albums.
* Cache loaded photos placeholder on the disk.