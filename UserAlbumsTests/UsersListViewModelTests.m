//
//  UsersListViewModelTests.m
//  UserAlbums
//
//  Created by Alexander Kozin on 28.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import <OCMock/OCMock.h>

#import "PSFUsersListViewModel.h"
#import "PSFDataDownloader.h"
#import <ReactiveCocoa/ReactiveCocoa.h>


SpecBegin(UsersListViewModel)

describe(@"UsersListViewModel", ^{
    it(@"check downloading data", ^{
        PSFUsersListViewModel *viewModel = [[PSFUsersListViewModel alloc] init];
        expect(viewModel.model).to.beNil();
        id dataDownloaderMock = OCMClassMock([PSFDataDownloader class]);
        RACSignal *fetchUsersListStub = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            [subscriber sendNext:RACTuplePack(nil, [NSData data])];
            return nil;
        }];
        [[[dataDownloaderMock stub] andReturn:fetchUsersListStub] fetchUsersList];
        viewModel.active = YES;
        // после установки флага активности происходит
        // загрузка данных для модели
        expect(viewModel.model).willNot.beNil();
        // массив с данными должен быть не пустым
        expect(viewModel.model).willNot.beEmpty();
    });
});

SpecEnd
