//
//  PSFAlbumModel.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSFAlbumModel : NSObject

@property (nonatomic, strong) NSNumber *albumId;
@property (nonatomic, strong) NSString *title;

@end
