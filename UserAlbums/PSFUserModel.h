//
//  PSFUserModel.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSFUserModel : NSObject

@property (nonatomic, assign) NSNumber *userId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;

@end
