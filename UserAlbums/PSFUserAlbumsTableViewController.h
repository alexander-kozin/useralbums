//
//  PSFUserAlbumsTableViewController.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PSFUserAlbumsViewModel;

@interface PSFUserAlbumsTableViewController : UITableViewController

@property (nonatomic, strong) PSFUserAlbumsViewModel *viewModel;

@end
