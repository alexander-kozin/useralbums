//
//  PFSSettings.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSFSettings : NSObject

- (NSURL *)usersListURL;
- (NSURL *)userAlbumsURLFor:(NSString *)userId;
- (NSURL *)albumPhotosURLFor:(NSString *)albumId;

+ (instancetype)sharedSettings;

@end
