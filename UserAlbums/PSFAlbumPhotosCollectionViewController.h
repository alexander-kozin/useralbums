//
//  PSFAlbumPhotosCollectionViewController.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PSFAlbumPhotosViewModel;

@interface PSFAlbumPhotosCollectionViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) PSFAlbumPhotosViewModel *viewModel;

@end
