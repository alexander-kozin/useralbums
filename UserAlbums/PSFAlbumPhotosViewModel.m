//
//  PSFAlbumPhotosViewModel.m
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFAlbumPhotosViewModel.h"
#import "PSFDataDownloader.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PSFAlbumPhotosViewModel()

@property (nonatomic, assign) BOOL loading;

@end

@implementation PSFAlbumPhotosViewModel
@dynamic model;

- (instancetype)initWithAlbumId:(NSString *)albumId title:(NSString *)title {
    if ( self = [super init] ) {
        _albumTitle = title;
        @weakify(self);
        [self.didBecomeActiveSignal subscribeNext:^(id x) {
            @strongify(self);
            self.loading = YES;
            RAC(self, model) = [[PSFDataDownloader fetchPhotosForAlbumWithId:albumId] doCompleted:^() {
                self.loading = NO;
            }];
        }];
        
    }
    return self;
}

@end
