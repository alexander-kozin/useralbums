//
//  PSFPhotoModel.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSFPhotoModel : NSObject

@property (nonatomic, strong) NSURL *thumbnailURL;

@end
