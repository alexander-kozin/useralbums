//
//  PSFLoadPhotoOperation.m
//  UserAlbums
//
//  Created by Alexander Kozin on 19.08.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>
#import "PSFLoadPhotoOperation.h"
#import "PSFDataDownloader.h"

@interface PSFLoadPhotoOperation() {
    BOOL operationIsFinished;
    BOOL operationIsExecuting;
    NSURLSession *session;
}

- (void)finishOperation;

@end


@implementation PSFLoadPhotoOperation

- (instancetype)initWithImageURL:(NSURL *)imageURL {
    if ( self = [super init] ) {
        self.imageURL = imageURL;
        operationIsFinished = NO;
    }
    return self;
}

- (void)main {
    NSLog(@"Operation is cancelled: %@", self.isCancelled ? @"YES" : @"NO");
    if ( self.isCancelled ) {
        [self finishOperation];
        return;
    }
    NSURL *imageFileURL = [self imageFileURL:self.imageURL];
    if ( [imageFileURL checkResourceIsReachableAndReturnError:nil] ) {
        NSData *imageData = [NSData dataWithContentsOfURL:imageFileURL];
        UIImage *image = [UIImage imageWithData:imageData];
        self.image = image;
        [self.resultsDelegate operationFinisedLoadPhoto:self];
        [self finishOperation];
    } else {
        if ( self.isCancelled ) {
            [self finishOperation];
            return;
        }
        session = [NSURLSession sessionWithConfiguration:NSURLSessionConfiguration.defaultSessionConfiguration delegate:self delegateQueue:nil];
        PSFLoadPhotoOperation * __weak weakSelf = self;
        NSURLSessionDownloadTask *task = [session downloadTaskWithURL:self.imageURL completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
            if ( !location || weakSelf.isCancelled ) {
                [weakSelf finishOperation];
                return;
            }
            // сохранение загруженного изображения в кэше
            [weakSelf moveFileAtURL:location toURL:imageFileURL];
            NSData *imageData = [NSData dataWithContentsOfURL:imageFileURL];
            UIImage *image = [UIImage imageWithData:imageData];
            weakSelf.image = image;
            [weakSelf.resultsDelegate operationFinisedLoadPhoto:weakSelf];
            [weakSelf finishOperation];
        }];
        [task resume];
    }
}

- (void)cancel {
    [super cancel];
    
    // отмена загрузки изображения
    [session invalidateAndCancel];
}

- (void)moveFileAtURL:(NSURL *)fromURL toURL:(NSURL *)toURL {
    NSFileManager *fm = [NSFileManager defaultManager];
    if ( [toURL checkResourceIsReachableAndReturnError:nil] ) {
        // если файл с таким имененем уже существует, то удаляю его
        [fm removeItemAtURL:toURL error:nil];
    }
    [fm moveItemAtURL:fromURL toURL:toURL error:nil];
}

- (NSURL *)imageFileURL:(NSURL *)imageNetworkURL {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *directoriesURLs = [fm URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSURL *cachesDirectoryURL = directoriesURLs.firstObject;
    NSString *imageName = imageNetworkURL.lastPathComponent;
    NSString *cachedImagesDirectoryName = @"thumbnails";
    NSURL *cachedImagesDirectoryURL = [cachesDirectoryURL URLByAppendingPathComponent:cachedImagesDirectoryName];
    if ( ![cachedImagesDirectoryURL checkResourceIsReachableAndReturnError:nil] ) {
        [fm createDirectoryAtURL:cachedImagesDirectoryURL withIntermediateDirectories:NO
                      attributes:nil error:nil];
    }
    return [cachedImagesDirectoryURL URLByAppendingPathComponent:imageName];
}

#pragma mark - Managing execution status

- (BOOL)isFinished {
    return operationIsFinished;
}

- (BOOL)isExecuting {
    return operationIsExecuting;
}

#pragma mark - Auxiliary methods

- (void)finishOperation {
    [self willChangeValueForKey:@"isExecuting"];
    operationIsExecuting = NO;
    [self didChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    operationIsFinished = YES;
    [self didChangeValueForKey:@"isFinished"];
    
    [session invalidateAndCancel];
}


@end
