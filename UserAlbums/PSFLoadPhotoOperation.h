//
//  PSFLoadPhotoOperation.h
//  UserAlbums
//
//  Created by Alexander Kozin on 19.08.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

@import UIKit;

@protocol PSFLoadPhotoOperationResultsDelegate;

@interface PSFLoadPhotoOperation : NSOperation <NSURLSessionTaskDelegate>

@property (nonatomic) UIImage *image;
@property (nonatomic) NSURL *imageURL;
@property (weak, nonatomic) id<PSFLoadPhotoOperationResultsDelegate> resultsDelegate;

- (instancetype)initWithImageURL:(NSURL *)imageURL;

@end


@protocol PSFLoadPhotoOperationResultsDelegate

- (void)operationFinisedLoadPhoto:(PSFLoadPhotoOperation *)operation;

@end

