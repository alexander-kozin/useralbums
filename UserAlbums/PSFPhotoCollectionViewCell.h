//
//  PSFPhotoCollectionViewCell.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSFLoadPhotoOperation.h"
@class PSFPhotoModel;

@interface PSFPhotoCollectionViewCell : UICollectionViewCell <PSFLoadPhotoOperationResultsDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) PSFPhotoModel *photoModel;
@end
