//
//  PSFUserAlbumsTableViewController.m
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "PSFUserAlbumsTableViewController.h"
#import "PSFAlbumModel.h"
#import "PSFUserAlbumsViewModel.h"
#import "PSFAlbumPhotosViewModel.h"
#import "PSFAlbumPhotosCollectionViewController.h"

@interface PSFUserAlbumsTableViewController ()

@end

@implementation PSFUserAlbumsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.viewModel.userName;
    
    @weakify(self);
    [RACObserve(self.viewModel, model) subscribeNext:^(id x) {
        @strongify(self);
        [self.tableView reloadData];
    }];
    self.viewModel.active = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [RACObserve(self.viewModel, loading) subscribeNext:^(NSNumber *loading) {
        if ( loading.boolValue ) {
            [SVProgressHUD show];
        } else {
            [SVProgressHUD dismiss];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.model.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserAlbumTableViewCell"
                                                            forIndexPath:indexPath];
    
    PSFAlbumModel *album = self.viewModel.model[indexPath.row];
    cell.textLabel.text = album.title;
    
    return cell;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedIndexPath = self.tableView.indexPathForSelectedRow;
    PSFAlbumModel *albumModel = self.viewModel.model[selectedIndexPath.row];
    NSString *albumId = [albumModel.albumId stringValue];
    NSString *albumTitle = albumModel.title;
    
    PSFAlbumPhotosViewModel *viewModel = [[PSFAlbumPhotosViewModel alloc] initWithAlbumId:albumId
                                                                                    title:albumTitle];
    PSFAlbumPhotosCollectionViewController *destinationVC = [segue destinationViewController];
    destinationVC.viewModel = viewModel;
}


@end
