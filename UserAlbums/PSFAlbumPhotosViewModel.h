//
//  PSFAlbumPhotosViewModel.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <ReactiveViewModel/ReactiveViewModel.h>

@interface PSFAlbumPhotosViewModel : RVMViewModel

@property (nonatomic, readonly) NSArray *model;
@property (nonatomic, readonly, strong) NSString *albumTitle;
@property (nonatomic, readonly, assign) BOOL loading;

- (instancetype)initWithAlbumId:(NSString *)albumId title:(NSString *)title;

@end
