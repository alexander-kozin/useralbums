//
//  PFSUsersListViewModel.m
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFUsersListViewModel.h"
#import "PSFDataDownloader.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PSFUsersListViewModel ()

@property (nonatomic, assign) BOOL loading;

@end

@implementation PSFUsersListViewModel
@dynamic model;

- (instancetype)init {
    if ( self = [super init] ) {
        @weakify(self);
        [self.didBecomeActiveSignal subscribeNext:^(id x) {
            @strongify(self);
            self.loading = YES;
            RAC(self, model) = [[PSFDataDownloader fetchUsersList] doCompleted:^{
                self.loading = NO;
            }];
        }];
    }
    return self;
}

@end
