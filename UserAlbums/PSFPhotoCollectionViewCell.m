//
//  PSFPhotoCollectionViewCell.m
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>
#import "PSFPhotoCollectionViewCell.h"
#import "PSFPhotoModel.h"
#import "PSFDataDownloader.h"
#import "PSFLoadPhotoOperation.h"

@interface PSFPhotoCollectionViewCell()

@property (weak, nonatomic) PSFLoadPhotoOperation *loadPhotoOperation;

@end

@implementation PSFPhotoCollectionViewCell

static NSOperationQueue *loadImagesQueue;

+ (void)initialize {
    if ( self == [PSFPhotoCollectionViewCell self] ) {
        loadImagesQueue = [[NSOperationQueue alloc] init];
    }
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder] ) {
        @weakify(self);
        [[RACObserve(self, photoModel.thumbnailURL) ignore:nil] subscribeNext:^(NSURL *url) {
            @strongify(self);
            NSLog(@"Queue: %p. Operations count: %ld", loadImagesQueue, loadImagesQueue.operationCount);
            PSFLoadPhotoOperation *operation = [[PSFLoadPhotoOperation alloc]
                                                initWithImageURL:self.photoModel.thumbnailURL];
            operation.resultsDelegate = self;
            // сохранение ссылки на операцию для последующей отмены
            self.loadPhotoOperation = operation;
            [loadImagesQueue addOperation:operation];

            NSLog(@"Queue: %p. Operations count: %ld", loadImagesQueue, loadImagesQueue.operationCount);

            // установка временного изображения-заместителя
            self.imageView.image = [UIImage imageNamed:@"photoPlaceholder"];
        }];
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.loadPhotoOperation cancel];
}

#pragma mark - Load photo operation delegate

- (void)operationFinisedLoadPhoto:(PSFLoadPhotoOperation *)operation {
    // если операция отменена или для данной ячейки  загружается новое изображение
    if ( [NSThread isMainThread] ) {
        if ( operation.isCancelled || operation.imageURL != self.photoModel.thumbnailURL ) {
            return;
        }
        self.imageView.image = operation.image;
    } else {
        [self performSelectorOnMainThread:@selector(operationFinisedLoadPhoto:) withObject:operation waitUntilDone:NO];
    }
}

@end
