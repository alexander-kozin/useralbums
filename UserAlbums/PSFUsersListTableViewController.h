//
//  PSFUsersListTableViewController.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSFUsersListTableViewController : UITableViewController

@end
