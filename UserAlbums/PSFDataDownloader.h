//
//  PSFDataDownloader.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RACSignal;

@interface PSFDataDownloader : NSObject

+ (RACSignal *)fetchUsersList;
+ (RACSignal *)fetchAlbumsForUserWithId:(NSString *)userId;
+ (RACSignal *)fetchPhotosForAlbumWithId:(NSString *)albumId;
+ (RACSignal *)fetchImage:(NSURL *)url;

@end
