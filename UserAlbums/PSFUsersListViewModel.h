//
//  PFSUsersListViewModel.h
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <ReactiveViewModel/ReactiveViewModel.h>

@interface PSFUsersListViewModel : RVMViewModel

@property (nonatomic, readonly, strong) NSArray *model;
@property (nonatomic, readonly, assign) BOOL loading;

@end
