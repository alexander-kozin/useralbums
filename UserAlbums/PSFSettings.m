//
//  PSFSettings.m
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFSettings.h"

@interface PSFSettings () {
    NSDictionary *settings;
}

- (NSURL *)baseURL;

@end


@implementation PSFSettings

+ (instancetype)sharedSettings {
    static PSFSettings *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PSFSettings alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if ( self = [super init] ) {
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSString *settingsFileName = [mainBundle pathForResource:@"Settings" ofType:@"plist"];
        settings = [[NSDictionary alloc] initWithContentsOfFile:settingsFileName];
    }
    return self;
}

- (NSURL *)baseURL {
    NSString *baseURLString = [settings objectForKey:@"baseURL"];
    return [NSURL URLWithString:baseURLString];
}

- (NSURL *)usersListURL {
    NSURL *baseURL = [self baseURL];
    NSURLComponents *urlComponents = [[NSURLComponents alloc]
                                      initWithURL:baseURL resolvingAgainstBaseURL:NO];
    NSString *usersListURLPartString = [settings objectForKey:@"usersURLPart"];
    urlComponents.path = usersListURLPartString;
    return urlComponents.URL;
}

- (NSURL *)userAlbumsURLFor:(NSString *)userId {
    NSURL *baseURL = [self baseURL];
    NSURLComponents *urlComponents = [[NSURLComponents alloc]
                                      initWithURL:baseURL resolvingAgainstBaseURL:NO];
    NSString *albumsURLPartString = [settings objectForKey:@"albumsURLPart"];
    urlComponents.path = albumsURLPartString;
    urlComponents.queryItems = @[[NSURLQueryItem queryItemWithName:@"userId" value:userId]];
    return urlComponents.URL;
}

- (NSURL *)albumPhotosURLFor:(NSString *)albumId {
    NSURL *baseURL = [self baseURL];
    NSURLComponents *urlComponents = [[NSURLComponents alloc]
                                      initWithURL:baseURL resolvingAgainstBaseURL:NO];
    NSString *photosURLPartString = [settings objectForKey:@"photosURLPart"];
    urlComponents.path = photosURLPartString;
    urlComponents.queryItems = @[[NSURLQueryItem queryItemWithName:@"albumId" value:albumId]];
    return urlComponents.URL;
}

@end
