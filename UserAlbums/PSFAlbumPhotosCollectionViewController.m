//
//  PSFAlbumPhotosCollectionViewController.m
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFAlbumPhotosCollectionViewController.h"
#import "PSFAlbumPhotosViewModel.h"
#import "PSFPhotoCollectionViewCell.h"
#import "PSFAlbumModel.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PSFAlbumPhotosCollectionViewController ()

@end

@implementation PSFAlbumPhotosCollectionViewController

static NSString * const reuseIdentifier = @"PhotoCollectionViewCell";

#pragma mark - UIViewController life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.viewModel.albumTitle;
    
    @weakify(self);
    [RACObserve(self.viewModel, model) subscribeNext:^(id x) {
        @strongify(self);
        [self.collectionView reloadData];
    }];
    self.viewModel.active = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [RACObserve(self.viewModel, loading) subscribeNext:^(NSNumber *loading) {
        if ( loading.boolValue ) {
            [SVProgressHUD show];
        } else {
            [SVProgressHUD dismiss];
        }
    }];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.viewModel.model.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PSFPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                                                 forIndexPath:indexPath];
    
    PSFPhotoModel *photoModel = self.viewModel.model[indexPath.item];
    cell.photoModel = photoModel;
    return cell;
}


#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

#pragma mark - UICollectionViewFlowLayoutDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat contentSizeWidth = self.collectionView.contentSize.width;
    CGFloat interItemSpacing = [(UICollectionViewFlowLayout *)self.collectionViewLayout minimumInteritemSpacing];
    UIEdgeInsets sectionInset = [(UICollectionViewFlowLayout *)self.collectionViewLayout sectionInset];
    CGFloat contentSizeWidthWithoutSpacing = contentSizeWidth - (interItemSpacing * 2) - sectionInset.left - sectionInset.right;
    CGFloat itemWidth = floor(contentSizeWidthWithoutSpacing / 2);
    return CGSizeMake(itemWidth, itemWidth);
}


@end
