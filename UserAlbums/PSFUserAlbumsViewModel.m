//
//  PSFUserAlbumsViewModel.m
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFUserAlbumsViewModel.h"
#import "PSFDataDownloader.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PSFUserAlbumsViewModel()

@property (nonatomic, assign) BOOL loading;

@end


@implementation PSFUserAlbumsViewModel
@dynamic model;

- (instancetype)initWithUserId:(NSString *)userId name:(NSString *)userName {
    if ( self = [super init] ) {
        _userName = userName;
        @weakify(self);
        [self.didBecomeActiveSignal subscribeNext:^(id x) {
            @strongify(self);
            self.loading = YES;
            RAC(self, model) = [[PSFDataDownloader fetchAlbumsForUserWithId:userId] doCompleted:^{
                self.loading = NO;
            }];
            
        }];
    }
    return self;
}

@end
