//
//  PSFUsersListTableViewController.m
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFUsersListTableViewController.h"
#import "PSFUsersListViewModel.h"
#import "PSFUserModel.h"
#import "PSFUserAlbumsViewModel.h"
#import "PSFUserAlbumsTableViewController.h"
#import <SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface PSFUsersListTableViewController () {
    PSFUsersListViewModel *viewModel;
}

@end

@implementation PSFUsersListTableViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder] ) {
        viewModel = [PSFUsersListViewModel new];
    }
    return self;
}

#pragma mark - UIViewController life cycle

- (void)viewDidLoad {
    [super viewDidLoad];

    @weakify(self)
    [RACObserve(viewModel, model) subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView reloadData];
    }];
    viewModel.active = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [RACObserve(viewModel, loading) subscribeNext:^(NSNumber *loading) {
        if ( loading.boolValue ) {
            [SVProgressHUD show];
        } else {
            [SVProgressHUD dismiss];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return viewModel.model.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserTableViewCell"
                                                            forIndexPath:indexPath];
    PSFUserModel *userModel = viewModel.model[indexPath.row];
    cell.textLabel.text = userModel.name;
    cell.detailTextLabel.text = userModel.email;
    
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedIndexPath = self.tableView.indexPathForSelectedRow;
    PSFUserModel *userModel = viewModel.model[selectedIndexPath.row];
    NSString *userId = [userModel.userId stringValue];
    NSString *userName = userModel.name;
    
    PSFUserAlbumsViewModel *albumsViewModel = [[PSFUserAlbumsViewModel alloc] initWithUserId:userId
                                                                                        name:userName];
    PSFUserAlbumsTableViewController *destinationVC = [segue destinationViewController];
    destinationVC.viewModel = albumsViewModel;
}


@end
