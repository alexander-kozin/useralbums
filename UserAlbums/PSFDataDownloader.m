//
//  PSFDataDownloader.m
//  UserAlbums
//
//  Created by Alexander Kozin on 26.06.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFDataDownloader.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "PSFSettings.h"
#import "PSFUserModel.h"
#import "PSFAlbumModel.h"
#import "PSFPhotoModel.h"

@interface PSFDataDownloader ()

+ (void)configureModel:(id)model withData:(NSDictionary *)data;
+ (NSURL *)imageFileURL:(NSURL *)imageNetworkURL;
+ (void)moveFileAtURL:(NSURL *)fromURL toURL:(NSURL *)toURL;

@end

@implementation PSFDataDownloader

+ (RACSignal *)fetchUsersList {
    NSURL *usersListURL = [[PSFSettings sharedSettings] usersListURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:usersListURL];
    return [[[[NSURLConnection rac_sendAsynchronousRequest:request]
             reduceEach:^id(NSURLResponse *response, NSData *data) {
                 return data;
             }] deliverOn:[RACScheduler mainThreadScheduler]]
            map:^id(NSData *data) {
                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                return [[[jsonData rac_sequence] map:^id(NSDictionary *userData) {
                    PSFUserModel *user = [PSFUserModel new];
                    [self configureModel:user withData:userData];
                    return user;
                }] array];
            }];
}

+ (RACSignal *)fetchAlbumsForUserWithId:(NSString *)userId {
    NSURL *userAlbumsURL = [[PSFSettings sharedSettings] userAlbumsURLFor:userId];
    NSURLRequest *request = [NSURLRequest requestWithURL:userAlbumsURL];
    return [[[[NSURLConnection rac_sendAsynchronousRequest:request]
            reduceEach:^id(NSURLResponse *response, NSData *data) {
                return data;
            }] deliverOn:[RACScheduler mainThreadScheduler]]
            map:^id(NSData *data) {
                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                return [[[jsonData rac_sequence] map:^id(NSDictionary *albumData) {
                    PSFAlbumModel *album = [PSFAlbumModel new];
                    [self configureModel:album withData:albumData];
                    return album;
                }] array];
            }];
}

+ (RACSignal *)fetchPhotosForAlbumWithId:(NSString *)albumId {
    NSURL *albumPhotosURL = [[PSFSettings sharedSettings] albumPhotosURLFor:albumId];
    NSURLRequest *request = [NSURLRequest requestWithURL:albumPhotosURL];
    return [[[[NSURLConnection rac_sendAsynchronousRequest:request]
            reduceEach:^(NSURLResponse *response, NSData *data) {
                return data;
            }] deliverOn:[RACScheduler mainThreadScheduler]]
            map:^id(NSData *data) {
                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                return [[[jsonData rac_sequence] map:^id(NSDictionary *photosData) {
                    PSFPhotoModel *photo = [PSFPhotoModel new];
                    [self configureModel:photo withData:photosData];
                    return photo;
                }] array];
            }];
}

+ (RACSignal *)fetchImage:(NSURL *)url {
    // кэширование изображений
    NSURL *imageFileURL = [self imageFileURL:url];
    if ( [imageFileURL checkResourceIsReachableAndReturnError:nil] ) {
        return [NSData rac_readContentsOfURL:imageFileURL options:0
                                   scheduler:[RACScheduler mainThreadScheduler]];
    } else {
        RACSignal *signal = [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            NSURLSession *session = NSURLSession.sharedSession;
            NSURLSessionDownloadTask *task = [session downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                if ( !location ) {
                    [subscriber sendError:error];
                    return;
                }
                // сохранение загруженного изображения в кэше
                [self moveFileAtURL:location toURL:imageFileURL];
                NSData *data = [NSData dataWithContentsOfURL:imageFileURL];
                [subscriber sendNext:data];
                [subscriber sendCompleted];
            }];
            [task resume];
            return [RACDisposable disposableWithBlock:^{
                [task cancel];
            }];
        }] deliverOn:[RACScheduler mainThreadScheduler]];
        return signal;
    }
}

+ (void)moveFileAtURL:(NSURL *)fromURL toURL:(NSURL *)toURL {
    NSFileManager *fm = [NSFileManager defaultManager];
    if ( [toURL checkResourceIsReachableAndReturnError:nil] ) {
        // если файл с таким имененем уже существует, то удаляю его
        [fm removeItemAtURL:toURL error:nil];
    }
    [fm moveItemAtURL:fromURL toURL:toURL error:nil];
}

+ (NSURL *)imageFileURL:(NSURL *)imageNetworkURL {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *directoriesURLs = [fm URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSURL *cachesDirectoryURL = directoriesURLs.firstObject;
    NSString *imageName = imageNetworkURL.lastPathComponent;
    NSString *cachedImagesDirectoryName = @"thumbnails";
    NSURL *cachedImagesDirectoryURL = [cachesDirectoryURL URLByAppendingPathComponent:cachedImagesDirectoryName];
    if ( ![cachedImagesDirectoryURL checkResourceIsReachableAndReturnError:nil] ) {
        [fm createDirectoryAtURL:cachedImagesDirectoryURL withIntermediateDirectories:NO
                                                 attributes:nil error:nil];
    }
    return [cachedImagesDirectoryURL URLByAppendingPathComponent:imageName];
}

+ (void)configureModel:(id)model withData:(NSDictionary *)data {
    if ( [model isKindOfClass:[PSFUserModel class]] ) {
        PSFUserModel *userModel = model;
        userModel.userId = [data objectForKey:@"id"];
        userModel.name = [data objectForKey:@"name"];
        userModel.email = [data objectForKey:@"email"];
    } else if ( [model isKindOfClass:[PSFAlbumModel class]] ) {
        PSFAlbumModel *albumModel = model;
        albumModel.albumId = [data objectForKey:@"id"];
        albumModel.title = [data objectForKey:@"title"];
    } else if ( [model isKindOfClass:[PSFPhotoModel class]] ) {
        PSFPhotoModel *photoModel = model;
        NSString *thumbnailURLString = [data objectForKey:@"thumbnailUrl"];
        NSURL *thumbnailURL = [NSURL URLWithString:thumbnailURLString];
        photoModel.thumbnailURL = thumbnailURL;
    } else {
        NSLog(@"Unknown model class: %@", NSStringFromClass([model class]));
        abort();
    }
}

@end
